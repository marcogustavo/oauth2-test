package br.com.origam.oauth2mysql.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
