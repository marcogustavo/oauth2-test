package br.com.origam.oauth2mysql.controller;

import br.com.origam.oauth2mysql.payload.PlayerResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/players")
public class PlayerController {

    @GetMapping
    public List<PlayerResponse> getPlayers() {
        List<PlayerResponse> players = new ArrayList<>();
        players.add(new PlayerResponse(1L, "Cristiano Ronaldo"));
        players.add(new PlayerResponse(2L, "Messi"));
        return players;
    }
}
