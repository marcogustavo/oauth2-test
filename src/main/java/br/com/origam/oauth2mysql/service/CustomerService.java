package br.com.origam.oauth2mysql.service;

import br.com.origam.oauth2mysql.model.Customer;
import br.com.origam.oauth2mysql.payload.CustomerResponse;
import br.com.origam.oauth2mysql.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public List<CustomerResponse> getCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream()
                .map(customer -> {
                    CustomerResponse customerResponse = new CustomerResponse();
                    customerResponse.setId(customer.getId());
                    customerResponse.setName(customer.getName());
                    return customerResponse;
                })
                .collect(Collectors.toList());
    }
}
