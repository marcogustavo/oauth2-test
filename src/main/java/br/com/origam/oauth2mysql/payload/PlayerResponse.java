package br.com.origam.oauth2mysql.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerResponse {

    private Long id;

    private String name;

    public PlayerResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
