package br.com.origam.oauth2mysql.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponse {

    private Long id;

    private String name;
}
