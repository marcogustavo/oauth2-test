package br.com.origam.oauth2mysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2MysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(Oauth2MysqlApplication.class, args);
	}

}
